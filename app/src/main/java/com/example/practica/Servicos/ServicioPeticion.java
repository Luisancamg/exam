package com.example.practica.Servicos;

import com.example.practica.ViewModels.Peticion_Discos;
import com.example.practica.ViewModels.Peticion_Login;
import com.example.practica.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/todosDiscos")
    Call<Peticion_Discos> EXTRAERDISCOS_CALL();


}
