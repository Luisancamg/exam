package com.example.practica;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica.Api.api;
import com.example.practica.Servicos.ServicioPeticion;
import com.example.practica.ViewModels.Peticion_Discos;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuNoticias extends AppCompatActivity {


    private TextView txt1;
    private TextView txtdiscos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_noticias);

        txt1 = (TextView) findViewById(R.id.txt1);
        txtdiscos = (TextView) findViewById(R.id.txtdiscos);

        txt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicioPeticion servicioPeticion = api.getApi(MenuNoticias.this).create(ServicioPeticion.class);
                Call<Peticion_Discos> peticionDiscosCall = servicioPeticion.EXTRAERDISCOS_CALL();
                peticionDiscosCall.enqueue(new Callback<Peticion_Discos>() {
                    @Override
                    public void onResponse(Call<Peticion_Discos> call, Response<Peticion_Discos> response) {
                        Peticion_Discos peticion = response.body();
                        if(response.body()==null){
                            Toast.makeText(MenuNoticias.this, "Error Intentelo mas tarde", Toast.LENGTH_LONG).show();
                            return;
                        }else{
                            txtdiscos.setText("Disco en Lista: "+peticion.id+peticion.nombre+peticion.album+peticion.anio);
                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Discos> call, Throwable t) {
                        Toast.makeText(MenuNoticias.this, "Error: no hay discos", Toast.LENGTH_LONG).show();

                    }
                });
            }
        });
    }
}
